import { Location } from './get-weather.service';
// import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

export interface Location {
  latitude: number;
  longtitude: number;
}

export interface RootObject {
  consolidated_weather: ConsolidatedWeather[];
  time: Date;
  sun_rise: Date;
  sun_set: Date;
  timezone_name: string;
  parent: any;
  sources: any[];
  title: string;
  location_type: string;
  woeid: number;
  latt_long: string;
  timezone: string;
}

export interface ConsolidatedWeather {
  id: any;
  weather_state_name: string;
  weather_state_abbr: string;
  wind_direction_compass: string;
  created: Date;
  applicable_date: string;
  min_temp: number;
  max_temp: number;
  the_temp: number;
  wind_speed: number;
  wind_direction: number;
  air_pressure: number;
  humidity: number;
  visibility: number;
  predictability: number;
}

@Injectable()
export class GetWeatherService {
  constructor(private http: HttpClient) {}

  getWoeid(location: Location) {
    // console.log('location ', location.latitude);
    const url =
      'https://www.metaweather.com//api/location/search/?lattlong=' +
      location.latitude +
      ',' +
      location.longtitude;
    return this.http.get(url).map(value => value[0].woeid);
  }

  getWeather(positionWoeid: number): Observable<RootObject> {
    console.log('GetWeather: ', positionWoeid);
    const date = new Date();
    const currentDate =
      date.getFullYear() +
      '/' +
      Number(date.getMonth() + 1) +
      '/' +
      date.getDate();
    const url2 = `https://www.metaweather.com//api/location/${positionWoeid}`;
    console.log('url2: ', url2);
    return <Observable<RootObject>>this.http.get(url2);
  }

  // getWeeatherIcon(symbol) {
  //   const url3 = `https://www.metaweather.com//api/static/img/weather/png/64/${symbol}.png`;
  //   return this.http.get(url3);
  // }
  // .subscribe(

  //   response => {
  //     console.log(response)
  //     const url2 = 'https://www.metaweather.com//api/location/' + response[0].woeid;
  //     return this.http.get(url2);
  //   }
  // );
}
