import { AppRoutingModule } from './../app.routing.module';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import { NgModule } from '@angular/core';
import { WeatherComponent } from './weather/weather.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {TemperatureChangePipe} from './weather/weather.pipe';

@NgModule({
  declarations: [
    MenuComponent,
    WeatherComponent,
    TemperatureChangePipe,
  ],
  imports: [
    MatToolbarModule,
    CommonModule,
    AppRoutingModule
  ],
  exports: [
    MenuComponent,
    WeatherComponent,
    AppRoutingModule,
  ],
})

export class HeaderModule {}
