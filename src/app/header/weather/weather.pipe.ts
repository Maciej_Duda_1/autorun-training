import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'temperature' })
export class TemperatureChangePipe implements PipeTransform {
  transform(value: number) {
    if (value === null) {
      return '';
    }
    return Math.round(value) + ' ℃';

  }
}
