import { Component, OnInit } from '@angular/core';
import { GetWeatherService, Location, ConsolidatedWeather, RootObject } from '../services/get-weather.service';
import { Observable, Subject } from 'rxjs';
import { mergeMap, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  weather: any;
  position: Location = {latitude: 0, longtitude: 0};
  locationSubject = new Subject<Location>();
  locationObservable: Observable<Location>;
  // woeid: Observable<number>;
  weatherObservable: Observable<ConsolidatedWeather>;
  // iconUrl: string;

  constructor(private getWeatherService: GetWeatherService) {
    this.locationObservable = this.locationSubject.asObservable();
  }

  ngOnInit() {
    this.getLocation();
    this.locationSubject.subscribe((cords) => {
      console.log('cords... ', cords);
      this.position = cords;
    });

    this.weatherObservable = this.locationObservable.pipe(
      mergeMap(
        (value) => {
          console.log(value);
          return this.getWeatherService.getWoeid(value);
        }
      ),
      mergeMap(
        (value) => {
          console.log(value);
          return this.getWeatherService.getWeather(value);
        }
      ),
      map((weather) => {
        return weather.consolidated_weather[0];
      }),
      map((consolidatedWeather) => {
        const iconUrl = `https://www.metaweather.com//static/img/weather/png/64/${consolidatedWeather.weather_state_abbr}.png`;
        consolidatedWeather.weather_state_abbr = iconUrl;
        return consolidatedWeather;
      })
    );

    // this.weatherObservable.subscribe( weather => this.weather = weather);

      // mergeMap(
      //   value => {console.log(value); return value}
      // )


    // this.weatherObservable = this.woeid.pipe(
    //   mergeMap(
    //     value => this.getWeatherService.getWeather(value)
    //   )
    // );
    // // this.woeid.subscribe( (value) => this.weather = this.getWeatherService.getWeather(value));

    // this.weatherObservable.subscribe( value => {
    //    console.log(value.consolidated_weather[0]);
    //    this.weather = value.consolidated_weather[0];
    //    return value.consolidated_weather[0]; });

    // this.woeid.subscribe( (woeid) => { console.log( 'jestesm!!', woeid); return this.getWeatherService.getWeather(woeid) }
    // );




    // this.locationObservable.mergeMap((location) => {
    //   return this.getWeatherService.getWoeid(this.position);
    // }).map((res) => {
    //   console.log('res ', res);
    // });
    // )
  }

  getLocation() {
    navigator.geolocation.getCurrentPosition (
      location => {
        this.locationSubject.next({
          latitude: location.coords.latitude,
          longtitude: location.coords.longitude
        });
        // console.log(this.locationSubject);
        // this.position.latitude = location.coords.latitude;
        // this.position.longtitude = location.coords.longitude;
        // this.getWeatherService.getWoeid(this.position).subscribe((response) => {

        // });
      },
      error => this.showError(error)
    );
  }

  showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            console.log('User denied the request for Geolocation.');
            break;
        case error.POSITION_UNAVAILABLE:
            console.log('Location information is unavailable.');
            break;
        case error.TIMEOUT:
            console.log('The request to get user location timed out.');
            break;
        case error.UNKNOWN_ERROR:
            console.log('An unknown error occurred.');
            break;
    }
}


}
