import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BitcoinDataComponent } from './bitcoin-data.component';

describe('BitcoinDataComponent', () => {
  let component: BitcoinDataComponent;
  let fixture: ComponentFixture<BitcoinDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BitcoinDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BitcoinDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
