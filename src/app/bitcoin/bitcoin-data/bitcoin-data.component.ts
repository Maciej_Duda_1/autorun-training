import { mergeMap, map } from 'rxjs/operators';
import { async } from '@angular/core/testing';
import { Observable, Subject, Subscription } from 'rxjs';
import { BitcoinService } from './../bitcoin-service/bitcoin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bitcoin-data',
  templateUrl: './bitcoin-data.component.html',
  styleUrls: ['./bitcoin-data.component.scss']
})
export class BitcoinDataComponent implements OnInit {
  multi: any[];
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Time';
  showYAxisLabel = true;
  yAxisLabel = 'Price';
  // view = [900, 700 ];




  marketChosenSubject = new Subject<any>();
  marketChosenObservable: Observable<any>;
  marketsDataObservable: Observable<any>;
  coinDataObservable: Observable<any>;
  data: any;
  componentObject: any;
  subscribtion: Subscription;
  marketsInterval: any;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;

  onSelect(event) {
    // console.log(event);
  }

  constructor(private bitcoinService: BitcoinService) {
    this.marketChosenObservable = this.marketChosenSubject.asObservable();
    this.componentObject = this;
    // Object.assign(this.componentObject, { multi: this.data });
  }

  ngOnInit() {
    this.marketsDataObservable = this.bitcoinService.getMarketsData();
    // this.coinDataObservable = this.bitcoinService.getBitcoinData();
    // this.coinDataObservable.subscribe(value => {
    //   this.data = value;
    //   Object.assign(this.componentObject, { multi: this.data });
    // });
    this.onMarketSelect();

    // this.subscribtion = this.bitcoinService.marketChanged
    //   .subscribe(
    //     marketData => Object.assign(this.componentObject, { multi: marketData })
    //   );

    // this.bitcoinService.getBitcoinData()
    //   .subscribe(
    //     value => {
    //       this.data = value;
    //       return Object.assign(this.componentObject, { multi: this.data })
    //     }
    //   );
  }

  // this.marketChosenSubject.next(this.bitcoinService.getBitcoinData());




  onMarketSelect(selectedMarket?) {
    clearInterval(this.marketsInterval);
    this.marketsInterval = setInterval(() => {
      this.bitcoinService.getBitcoinData(selectedMarket).subscribe(
        value => {
          this.data = value;
          Object.assign(this.componentObject, { multi: this.data });
        }
      );
    }, 10000);
  }
}




    // this.marketChosenSubject.next(selectedMarket);
    // this.coinDataObservable = this.marketChosenObservable.pipe(
    //   mergeMap(
    //     (market) => this.bitcoinService.getBitcoinData(market)
    //   ),
    //   map(
    //     (marketData) => {
    //       console.log(marketData);
    //       this.data = marketData;
    //       return Object.assign(this.componentObject, { multi: this.data });
    //     }
    //   )
    // );

    // this.bitcoinService.getBitcoinData(selectedMarket).pipe(
    //   mergeMap(marketData => {
    //     console.log('hello: ', marketData);
    //     this.data = marketData;
    //     return Object.assign(this.componentObject, { multi: this.data });
    //   })
    // );

