import { BitcoinRoutingModule } from './bitcoin.routing.module';
import { BitcoinDataComponent } from './bitcoin-data/bitcoin-data.component';
// import { AppRoutingModule } from './../app.routing.module';
import { NgModule } from '@angular/core';
import { BitcoinService } from './bitcoin-service/bitcoin.service';
import { CommonModule } from '@angular/common';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [BitcoinDataComponent],
  imports: [BitcoinRoutingModule, CommonModule, NgxChartsModule, MatSelectModule, MatFormFieldModule],
  exports: [BitcoinDataComponent],
  providers: [BitcoinService]
})
export class BitcoinModule { }
