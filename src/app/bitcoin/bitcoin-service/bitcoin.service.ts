import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable()

export class BitcoinService {

  // marketChanged = new Subject<any>();

  constructor(private http: HttpClient) { }

  getMarketsData() {
    const urlMarketsData = 'http://api.bitcoincharts.com/v1/markets.json';
    return this.http.get(urlMarketsData).map(data => { console.log(data); return data; });
  }

  getBitcoinData(symbolOfMarket = 'bitbayPLN') {
    const selectedData = [{ name: "BTC/PLN", "series": [] }];
    const date = Math.floor(Date.now() / 1000) - 3600;
    // console.log('date: ', date);
    const url = `http://api.bitcoincharts.com/v1/trades.csv?symbol=${symbolOfMarket}&start=${date}`;
    // console.log('url ', url);
    return this.http.get(url, { responseType: 'text' }).map(data => {
      let singleObjectsArray = [];
      const splitDataArray = data.split(/[\n]/g);
      singleObjectsArray = splitDataArray.map(singleData => singleData.split(/[,]/g));
      // console.log(singleObjectsArray);
      singleObjectsArray.map((singleValueArray, index) => {
        const dateOfTransaction = new Date(+singleValueArray[0] * 1000);
        const dataObject = {
          name: `${dateOfTransaction.getHours() + (' : ' + dateOfTransaction.getMinutes()) + ' : ' + dateOfTransaction.getSeconds()}    Amount : ${singleValueArray[2]}`,
          value: +singleValueArray[1]
        };
        selectedData[0].series.push(dataObject);
      });
      console.log('serwis się zgłasza: ', selectedData);
      // this.marketChanged.next(selectedData);
      return selectedData;
    });
  }
}

      // const splitData = data.split(/[\n,]/g);
      // const selectedData = [{ name: "BTC/PLN", "series": [] }];
      // splitData.map((singleData, index) => {
      //   if (index % 3 === 1) {
      //     const dataObject = {
      //       name: `BitcoinValue ${index}`,
      //       value: +singleData
      //     };
      //     selectedData[0].series.push(dataObject);
      //   }
      // });
      // return selectedData;
