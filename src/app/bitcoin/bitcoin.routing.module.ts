import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BitcoinDataComponent } from './bitcoin-data/bitcoin-data.component';

const bitcoinRoutes: Routes = [
  { path: '', component: BitcoinDataComponent }
  // , children: [
  //   {path: '', component: RecipeStartComponent},
  //   {path: 'new', component: RecipeEditComponent, canActivate: [AuthGuard]},
  //   {path: ':id', component: RecipeDetailComponent},
  //   {path: ':id/edit', component: RecipeEditComponent, canActivate: [AuthGuard]},
  // ]},
];


@NgModule({
  imports: [
    RouterModule.forChild(bitcoinRoutes)
  ],
  exports: [RouterModule],
  providers: []
})

export class BitcoinRoutingModule {

}
